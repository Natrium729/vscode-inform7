Version 1/190817 of Sample Extension by Nathanaël Marion begins here.

"This is the rubric of the extension."

"give some credits here"

Chapter 1 - The naming action

Naming is an action applying to one visible thing.
Understand "name [something]" as naming.

Report naming:
	say "This is [a noun].".

Sample extension ends here.

---- DOCUMENTATION ----

This is the documentation of the extension.

Chapter: A chapter

Section: With a section

Here is an Inform excerpt:

	Instead of naming the unnameable:
		say "You cannot name what cannot be named."

Example: * An example - The documentation can contain examples.

	*:"Naming mushrooms"

	Chapter 1 - The scenario

	The forest is a room.
	The mushroom is a thing in forest.

	Instead of naming the mushroom:
		say "It's a fly amanita!";
		now the printed name of the mushroom is "fly amanita".

	Chapter 2 - Testing commands

	Test me with "take mushroom / name mushroom".
