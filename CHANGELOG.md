# Change Log

## 1.3.0 (2024-01-26)

### Added

- Headings can now be folded in Inform 7 source files. Thanks @floriancargoet!

## 1.2.0 (2021-04-09)

### Added

- The commands to compile and release stories are available (in the palette or at the top right of windows) with Inform 7 extension files. In that case, the extension will check if the extension is project-specific (in a `.materials` folder) and compile the associated project.
- The `openStoryAfterCompilation` setting can take a value of `"vscode"` so that compiled stories are opened in a VS Code tab. The new `openStoryColumn` setting will make the story open in the specified column after compilation, if `openStoryAfterCompilation` is set to `"vscode"`.

### Changed

- The location of the buttons in the toolbar has been adjusted to follow VS Code's guidelines: the "compile" button is always shown (and releases the story for testing when `Alt` is pressed) and the "release" button has been moved to the `...` submenu.

### Fixed

- The extension is activated when opening an Inform extension (`.i7x` file).
- The default path of ni on macOS was not pointing at the right location and has been corrected.
- When compiling an Inform project, the output of ni and the encountered errors are now correctly reported in the Terminal and Problems panels on macOS (and likely Linux, too). Thanks to @floriancargoet for this fix!
- If a ni error message points to multiple locations in the source, the error will correctly be reported at each of these locations (in the Problems panel and the source) instead of just the first one.
- If a ni error message do not contain a location, it will now be reported at the first line, so that it appears in the Problems panel and in the source. (#11)
- Inform 7 words can contain Latin-1 characters, so the word completion will now suggest words containing hyphens or accented characters in full.
- There's no more an error notification saying that `${workspaceFolder}` cannot be resolved when compiling an Inform file with no workspace opened.

## 1.1.0 (2020-09-24)

### Added

- Add commands to compile and release stories. New settings has been added to open the story automatically after compilation and to specify the paths of the compilers (Default Inform installation is used by default).

## 1.0.0 (2020-07-09)

- Initial release
