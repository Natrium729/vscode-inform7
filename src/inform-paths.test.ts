import { globalStorage } from "./global-storage"
import { getNiPath, getCblorbPath } from "./inform-paths"

// Mocking properties of a global is complicated… jest.mock() doesn't work.
// We need to copy/restore the whole property definition, not just the raw value.
const platformDescriptor = Object.getOwnPropertyDescriptor(process, "platform")!

type Platform = typeof process.platform
function mockPlatform(platform: Platform) {
	Object.defineProperty(process, "platform", {
		...platformDescriptor,
		value: platform,
	})
}
function restorePlatform() {
	Object.defineProperty(process, "platform", platformDescriptor)
}

describe("getNiPath", () => {
	afterEach(() => {
		restorePlatform()
	})

	test("it returns the correct version on OSX", () => {
		// Pretend to be on OSX
		mockPlatform("darwin")

		expect(getNiPath()).toBe("/Applications/Inform.app/Contents/MacOS/ni")
		expect(getNiPath("6L38")).toBe("/Applications/Inform.app/Contents/MacOS/6L38/ni")
	})

	test("it returns the correct version on Linux", () => {
		mockPlatform("linux")
		const oldInstallPath = globalStorage.ideInstallLocation
		globalStorage.ideInstallLocation = "/var/lib/flatpak/app/com.inform7.IDE/current/active"
		const oldInformVersion = globalStorage.installedInformVersion
		globalStorage.installedInformVersion = "10.1.2"

		// We can't check the full path because it's determined dynamically
		// (it can be different from install to install),
		// so we just check the end of the path.

		// The current version.
		expect(getNiPath()).toMatch(/inform7-ide\/inform7$/)
		// A version that happens to be the current one.
		expect(getNiPath("10.1")).toMatch(/inform7-ide\/inform7$/)
		// A retrospective version
		expect(getNiPath("6L38")).toMatch(/retrospective\/6L38\/ni$/)

		// TODO: Add test for a retrospective version with a numbering other than
		// digit-letter-digit-digit.
		// (There aren't any at the time of writing.)

		// Restore the global storage.
		globalStorage.ideInstallLocation = oldInstallPath
		globalStorage.installedInformVersion = oldInformVersion
	})
})

describe("getCblorbPath", () => {
	afterEach(() => {
		restorePlatform()
	})

	test("it returns the correct version on Linux", () => {
		mockPlatform("linux")
		const oldInstallPath = globalStorage.ideInstallLocation
		globalStorage.ideInstallLocation = "/var/lib/flatpak/app/com.inform7.IDE/current/active"
		const oldInformVersion = globalStorage.installedInformVersion
		globalStorage.installedInformVersion = "10.1.2"

		// We can't check the full path on Linux because it's determined dynamically
		// (it can be different from install to install),
		// so we just check the end of the path.

		// The current version.
		expect(getCblorbPath()).toMatch(/inform7-ide\/inblorb$/)
		// A version that happens to be the current one.
		expect(getCblorbPath("10.1")).toMatch(/inform7-ide\/inblorb$/)
		// A retrospective version
		expect(getCblorbPath("6L38")).toMatch(/retrospective\/6L38\/cBlorb$/)

		// TODO: Add test for a retrospective version with a numbering other than
		// digit-letter-digit-digit.
		// (There aren't any at the time of writing.)

		// Restore the global storage.
		globalStorage.ideInstallLocation = oldInstallPath
		globalStorage.installedInformVersion = oldInformVersion
	})
})
