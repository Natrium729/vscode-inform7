// This script is used only when running tests from the CLI with `npm test`.
import * as path from "path"

import { runTests } from "vscode-test"

async function main() {
	try {
		// The folder containing the Extension Manifest package.json.
		// Passed to `--extensionDevelopmentPath`.
		const extensionDevelopmentPath = path.resolve(__dirname, "../../")

		// The path to the test runner.
		// Passed to --extensionTestsPath.
		const extensionTestsPath = path.resolve(__dirname, "./jest-test-runner")

		// Download VS Code, unzip it and run the tests.
		await runTests({
			extensionDevelopmentPath,
			extensionTestsPath,
			// I could only get the tests to run if I disable the other extensions (@floriancargoet)
			launchArgs: ["--disable-extensions"],
		})
	} catch (err) {
		console.error("Failed to run tests")
		process.exit(1)
	}
}

main()
