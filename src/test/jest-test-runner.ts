import { runCLI } from 'jest'
import type { Config } from '@jest/types'
import * as path from 'path'

const rootDir = path.resolve(__dirname, '../../')
const fromRoot = (...subPaths: string[]): string => path.resolve(rootDir, ...subPaths)

const jestConfig: Config.Argv = {
	// Pretend Jest is called from the CLI.
	$0: "",
	_: [],
	// Configuration as CLI arguments.
	rootDir,
	roots: [ '<rootDir>/src' ],
	transform: JSON.stringify({ '^.+\\.ts$': 'ts-jest' }),
	runInBand: true, // Required due to the way the "vscode" module is injected.
	testRegex: '\\.(test|spec)\\.ts$',
	// An environment that exposes VS Code as a global.
	testEnvironment: fromRoot('out/test/jest-vscode-environment.js'),
	// Creates a mock from "vscode" which returns global.vscode.
	setupFilesAfterEnv: [fromRoot('out/test/jest-vscode-framework-setup.js')],
	moduleFileExtensions: [ 'ts', 'tsx', 'js', 'jsx', 'json', 'node' ],
}

export type TestRunnerCallback = (error: Error | null, failures?: any) => void

export async function run(testRoot: string, reportTestResults: TestRunnerCallback) {
	try {
		const { results } = await runCLI(jestConfig, [rootDir])

		for (const testFile of results.testResults) {
			// Log passed tests first.
			const passedTests = testFile.testResults
				.filter(assertionResult => assertionResult.status === "passed")
			for (const { ancestorTitles, title, status } of passedTests) {
				console.info(`  ● ${ancestorTitles} › ${title} (${status})`)
			}
			// Log failures last since we have a nicely formatted message for all of them.
			if (testFile.failureMessage) {
				console.error(testFile.failureMessage)
			}
		}
		reportTestResults(null, results.numFailedTests)
	} catch (err) {
		reportTestResults(err, 0)
	}
}
