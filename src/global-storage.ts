// TODO: We could put the various managers here too,
// instead of passing them everywhere as argument?

interface GlobalStorage {
	ideInstallLocation: string | null,
	installedInformVersion: string | null,
}

export const globalStorage: GlobalStorage = {
	ideInstallLocation: null,
	installedInformVersion: null,
}
