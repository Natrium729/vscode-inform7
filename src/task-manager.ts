import path = require("path")

import * as vscode from "vscode"

import { getCblorbPath, getExternalPath, getInform6Path, getInternalPath, getNiPath } from "./inform-paths"
import { InformProjectManager } from "./inform-project"
import { ProcessManager } from "./process-manager"
import { NiTaskDefinition, isNiTaskDefinition } from "./task-providers/ni-task-provider"
import { Inform6nTaskDefinition, Inform6nTaskProvider, isInform6nTaskDefinition } from "./task-providers/inform6n-task-provider"
import { CblorbTaskDefinition, CblorbTaskProvider, isCblorbTaskDefinition } from "./task-providers/cblorb-task-provider"
import { ManualProgress } from "./utils/manual-progress"
import { stringToViewColumn } from "./utils/view-column"
import { NiOutputParser, ProgressEvent, ErrorEvent } from "./output-parser"

export class TaskManager {
	readonly compilationProgresses = new Map<string, ManualProgress>()

	private processManager: ProcessManager
	private projectManager: InformProjectManager
	private disposables: vscode.Disposable[] = []

	constructor(processManager: ProcessManager, projectManager: InformProjectManager) {
		this.processManager = processManager
		this.projectManager = projectManager

		// Execute Inform 6 when ni finishes.
		vscode.tasks.onDidEndTaskProcess(async (event) => {
			const niDefinition = event.execution.task.definition
			if (!isNiTaskDefinition(niDefinition)) {
				return
			}
			if (!(niDefinition.toInform6 ?? true)) {
				this.removeProgress(niDefinition.project)
				return
			}
			if (event.exitCode) {
				// TODO: Show message?
				this.removeProgress(niDefinition.project)
				return
			}

			this.compilationProgresses.get(niDefinition.project)?.update({
				message: "Running result through Inform 6",
				increment: 0
			})
			const mode = niDefinition.mode ?? "test"

			const i6nDefinition: Inform6nTaskDefinition = {
				type: Inform6nTaskProvider.Inform6nTaskType,
				project: niDefinition.project,
				debug: mode === "test" || mode === "release-test"
			}

			if (niDefinition.format) {
				i6nDefinition.format = niDefinition.format
			}

			const blorb = niDefinition.blorb ?? await projectManager.getOutputSettingCreateBlorb(niDefinition.project)
			if (mode !== "test" && blorb) {
				i6nDefinition.toCblorb = blorb
			} else {
				i6nDefinition.toCblorb = false
			}

			const inform6nTask = await this.getInform6nTask(i6nDefinition)

			if (!inform6nTask) {
				// Happens when we can't get the path to inform6
				// (i.e. Inform is not installed).
				//
				// TODO: Show a message to the user?
				// I think it can only happen when Inform is not installed,
				// at which point the user already got a message
				// earlier in the task chain.
				return
			}

			vscode.tasks.executeTask(inform6nTask)
		}, this, this.disposables)

		/* Open the story when Inform 6 finishes, or execute cBlorb if it's a release. */
		vscode.tasks.onDidEndTaskProcess(async (event) => {
			const i6nDefinition = event.execution.task.definition
			if (!isInform6nTaskDefinition(i6nDefinition)) {
				return
			}
			if (event.exitCode) {
				// TODO: Show message?
				this.removeProgress(i6nDefinition.project)
				return
			}

			const toCblorb = i6nDefinition.toCblorb ?? false
			if (!toCblorb) {
				this.removeProgress(i6nDefinition.project)
				const config = vscode.workspace.getConfiguration("inform7")

				// We are not releasing, so open the story file if settings say so.
				const openStory = config.get<string>("openStoryAfterCompilation", "no")
				if (openStory !== "no") {
					const format = i6nDefinition.format ?? await this.projectManager.getOutputSettingStoryFormat(i6nDefinition.project)
					const storyUri = vscode.Uri.file(path.join(
						i6nDefinition.project,
						`Build/output.${format}`
					))

					if (openStory === "external") {
						vscode.env.openExternal(storyUri)
					} else if (openStory === "vscode") {
						vscode.commands.executeCommand("vscode.open", storyUri, {
							preview: false,
							viewColumn: stringToViewColumn(
								config.get<string>("openStoryColumn", "active")
							)
						})
					}
				}

				return
			}

			this.compilationProgresses.get(i6nDefinition.project)?.update({
				message: "Binding up story into a Blorb file",
				increment: 0
			})

			const format = i6nDefinition.format ?? await projectManager.getOutputSettingStoryFormat(i6nDefinition.project)

			const cblorbTask = await this.getCblorbTask({
				type: CblorbTaskProvider.CblorbTaskType,
				project: i6nDefinition.project,
				format: format
			})

			if (!cblorbTask) {
				// Happens when we can't get the path to cBlorb
				// (i.e. Inform is not installed).
				//
				// TODO: Show a message to the user?
				// I think it can only happen when Inform is not installed,
				// at which point the user already got a message
				// earlier in the task chain.
				return
			}

			vscode.tasks.executeTask(cblorbTask)
		}, this, this.disposables)

		// Mark compilation progress complete when cBlorb finishes.
		vscode.tasks.onDidEndTaskProcess(async (event) => {
			const cblorbDefinition = event.execution.task.definition
			if (!isCblorbTaskDefinition(cblorbDefinition)) {
				return
			}
			this.removeProgress(cblorbDefinition.project)
		}, this, this.disposables)
	}

	removeProgress(key: string) {
		const progress = this.compilationProgresses.get(key)
		if (progress) {
			progress.dispose()
			this.compilationProgresses.delete(key)
		}
	}

	async getNiTask(definition: NiTaskDefinition): Promise<vscode.Task | undefined> {
		const informVersion = definition.informVersion ?? await this.projectManager.getOutputSettingCompilerVersion(definition.project)

		const niPath = getNiPath(informVersion)
		if (!niPath) {
			return
		}

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)

		const args = [
			"--internal",
			getInternalPath(informVersion),
			"--external",
			getExternalPath(),
			"--project",
			definition.project,
			`--format=${format}`
		]

		const mode = definition.mode ?? "test"

		const noRng = definition.noRng ?? await this.projectManager.getOutputSettingNoRng(definition.project)
		if (mode === "test" && noRng) {
			args.push("--rng")
		}

		if (mode === "release" || mode === "release-test") {
			args.push("--release")
		}

		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`compile ${path.basename(definition.project)}`,
			"inform7",
			new vscode.CustomExecution(async () => {
				return new NiTaskTerminal(definition.project, niPath, args, this.processManager, this)
			}),
			"$inform7"
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	async getInform6nTask(definition: Inform6nTaskDefinition): Promise<vscode.Task | undefined> {
		const inform6Path = getInform6Path()

		if (!inform6Path) {
			return
		}

		// TODO: -k arg to output I6 debug info.
		const args = [
			"-w", // Ignore warnings,
			"-c", // No code excerpt in error messages.
			"-E1", // Microsoft error style (for the problem matcher).
			"+include_path=../Source,./",
		]

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)
		switch (format) {
			case "z5":
				args.push("-v5")
				break
			case "z8":
				args.push("-v8")
				break
			case "ulx":
				args.push("-G")
				break
		}

		if (definition.debug ?? true) {
			args.push("-SD") // Enable strict and debug.
		} else {
			args.push("-~S~D") // Disable strict and debug.
		}

		/* We use path.join to have absolute paths, so that the problem matcher has absolute paths too. */
		args.push(path.join(definition.project, "Build/auto.inf"))
		args.push(path.join(definition.project, `Build/output.${format}`))

		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`compile ${path.basename(definition.project)}`,
			"inform7",
			new vscode.ProcessExecution(inform6Path, args, {
				cwd: path.join(definition.project, "Build"),
			}),
			"$inform6n"
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	async getCblorbTask(definition: CblorbTaskDefinition): Promise<vscode.Task | undefined> {
		const cblorbPath = getCblorbPath()

		if (!cblorbPath) {
			return
		}

		const args: string[] = []

		switch (process.platform) {
			case "linux":
				args.push("-unix")
				break
			case "win32":
				args.push("-windows")
				break
		}

		const format = definition.format ?? await this.projectManager.getOutputSettingStoryFormat(definition.project)
		let outputExtension: "zblorb" | "gblorb"
		switch (format) {
			case "z5":
			case "z8":
				outputExtension = "zblorb"
				break
			case "ulx":
				outputExtension = "gblorb"
				break
		}

		args.push(
			"Release.blurb",
			`Build/output.${outputExtension}`
		)

		const task = new vscode.Task(
			definition,
			vscode.TaskScope.Workspace,
			`release ${path.basename(definition.project)}`,
			"inform7",
			new vscode.ProcessExecution(cblorbPath, args, {
				cwd: definition.project,
			})
		)
		task.presentationOptions = {
			reveal: vscode.TaskRevealKind.Never,
		}
		return task
	}

	dispose() {
		for (const [key, item] of this.compilationProgresses) {
			item.dispose()
		}
		this.compilationProgresses.clear()
		for (const disposable of this.disposables) {
			disposable.dispose()
		}
	}
}

class NiTaskTerminal implements vscode.Pseudoterminal {
	private writeEmitter = new vscode.EventEmitter<string>()
	onDidWrite: vscode.Event<string> = this.writeEmitter.event
	private closeEmitter = new vscode.EventEmitter<number>()
	onDidClose: vscode.Event<number> = this.closeEmitter.event
	private completion = 0

	private projectPath: string
	private niPath: string
	private args: string[]
	private processManager: ProcessManager
	private taskManager: TaskManager

	constructor(projectPath: string, niPath: string, args: string[], processManager: ProcessManager, taskManager: TaskManager) {
		this.projectPath = projectPath
		this.niPath = niPath
		this.args = args
		this.processManager = processManager
		this.taskManager = taskManager
	}

	open() {
		const progress = new ManualProgress()
		this.taskManager.compilationProgresses.set(this.projectPath, progress)
		progress.start({
			location: vscode.ProgressLocation.Notification,
			title: "Inform 7",
			cancellable: false
		})
		this.writeln(`${this.niPath} ${this.args.join(" ")}`)

		const niProcess = this.processManager.spawn(this.niPath, this.args, {
			"cwd": vscode.workspace.getWorkspaceFolder(vscode.Uri.file(this.projectPath))?.uri.fsPath
		})
		// We don't parse stdout and only redirect it to the terminal.
		niProcess.stdout?.on("data", (data) => {
			// Convert \n and \r\n to \r\n (expected by VS Code terminal).
			this.writeEmitter.fire((data as Buffer).toString("latin1").replace(/\r?\n/g, "\r\n"))
		})

		// stderr contains errors and progress updates, we use a dedicated parser to extract and normalize them.
		if (niProcess.stderr) {
			const outputParser = new NiOutputParser()
			outputParser.parseStream(niProcess.stderr)

			// Unhandled text, written as-is.
			outputParser.onText((text: string) => {
				this.writeEmitter.fire(text)
			})
			// Progress updates.
			outputParser.onProgress((progressEvent: ProgressEvent) => {
				const increment = progressEvent.progress - this.completion
				this.completion = progressEvent.progress
				this.taskManager.compilationProgresses.get(this.projectPath)?.update({
					increment,
					message: progressEvent.step
				})
				this.writeln(progressEvent.original)
			})
			// Errors.
			outputParser.onError((errorEvent: ErrorEvent) => {
				if (errorEvent.heading) {
					this.writeln(errorEvent.heading)
				}
				let location = errorEvent.location
				if (!location) {
					/* If there is no location (it's a "global" error),
					we report it on line 1 of story.ni so that it appears in the Problems panel. */
					location = {
						filePath: path.join(this.projectPath, "Source/story.ni"),
						line: "1"
					}
				}
				/* Ni writes "source text" when the error is in story.ni (rather than in an extension),
				so we replace it with the actual path of the source. */
				if (location.filePath === "source text") {
					location.filePath = path.join(this.projectPath, "Source/story.ni")
				}
				// Format the error so that it's parsed by a problem matcher.
				const formattedError = `  >--> ${errorEvent.message} (Error in ${location.filePath}, line ${location.line})`
				this.writeln(formattedError)
			})
		}

		niProcess.on("exit", (code) => {
			/* code === null means the process terminated because of a signal. We consider it a failure are exit with 1 instead. */
			this.closeEmitter.fire(code ?? 1)

			// Allow consumers to read the data written while handling the stderr "end" event.
			process.nextTick(() => this.dispose())
		})

	}

	close() {
		// TODO: interrupt Inform process?
	}

	dispose() {
		this.writeEmitter.dispose()
		this.closeEmitter.dispose()
	}

	writeln(line: string) {
		/* VS Code requires both CR and LF in its terminal, no matter the operating system. */
		this.writeEmitter.fire(line + "\r\n")
	}
}
