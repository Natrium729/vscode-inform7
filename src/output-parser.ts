import * as vscode from "vscode"
import { Readable } from "stream"

export type ProgressEvent = {
	progress: number,
	step: string,
	original: string // As printed by ni, newlines included.
}

export type ErrorEvent = {
	heading: string, // Optional heading (e.g. "In Part 2").
	message: string, // Error message without file & line.
	location?: {
		filePath?: string,
		line: string
	},
	original: string // As printed by ni, newlines included.
}

export class NiOutputParser {

	private progressEmitter = new vscode.EventEmitter<ProgressEvent>()
	onProgress: vscode.Event<ProgressEvent> = this.progressEmitter.event

	private errorEmitter = new vscode.EventEmitter<ErrorEvent>()
	onError: vscode.Event<ErrorEvent> = this.errorEmitter.event

	// Emits every line which is not a progress update or an error.
	private textEmitter = new vscode.EventEmitter<string>()
	onText: vscode.Event<string> = this.textEmitter.event

	// We may receive data in chunks, not line by line. We cumulate it and act when we receive a new line.
	private cumulatedOutput = ""

	// Error lines are parsed in batch so we need to store them.
	private errorLines: string[] = []

	parseString(text: string) {
		this.cumulatedOutput += text
		// Continue cumulating output until we have at least one newline.
		if (!this.cumulatedOutput.includes("\n")) {
			return
		}
		const lines = this.cumulatedOutput.split(/\r?\n/g)
		// The last line may not be complete, we keep it as the next cumulated output.
		this.cumulatedOutput = lines.pop() ?? ""
		for (const line of lines) {
			this.handleOutputLine(line)
		}
	}

	parseStream(stream: Readable) {
		stream.on("data", (data) => {
			this.parseString((data as Buffer).toString("latin1"))
		})

		stream.on("end", () => {
			// In case the output ended while reading an error paragraph, handle it now.
			if (this.errorLines.length > 0) {
				this.handleErrorLines()
			}
		})
	}

	handleOutputLine(line: string) {
		if (line.startsWith("++ ")) {
			// It's a progress line.
			// If we have an error block waiting, handle it first.
			if (this.errorLines.length > 0) {
				this.handleErrorLines()
				this.errorLines = []
			}
			this.handleProgress(line)
		}
		else {
			// It's a line inside an error block, store it until we receive the whole block.
			this.errorLines.push(line)
		}
	}

	handleProgress(progressLine: string) {
		/* It can be a progress message (e.g. "++ 5% (Analysing sentences)")
		or a final message (e.g. "++ Ended: Translation failed: 3 problems found"). */
		const progressRe = /\+\+ (?<progress>\d+)% \((?<step>[^)]+)\)/
		const match = progressRe.exec(progressLine)
		if (match?.groups) {
			this.progressEmitter.fire({
				progress: parseInt(match.groups.progress, 10),
				step: match.groups.step,
				original: progressLine
			})
		}
		else {
			// textEmitter is supposed to emit raw text so we add the new line that we removed previously
			this.textEmitter.fire(progressLine + "\r\n")
		}
	}

	handleErrorLines() {
		/* There might be multiple errors at once, we need to group the lines by error.
		A group of errors may start with a heading on multiple lines (e.g. In part 1, In the main text…).
		Then, the details of the first error, each starting with "  >-->", are on the next lines, all indented with 4 spaces.
		When the indentation becomes less than 4, we are on the next error
		(it can be 0 or 1 for a new location info or 2 for the next "arrow"). */
		let currentErrorHeading: string[] = []
		let currentErrorDetails: string[] = []
		for (const line of this.errorLines) {
			// If the line starts with strictly less than 4 spaces.
			if (line.match(/^ {0,3}[^ ]/)) {
				// If we are reading the details of a new error.
				if (line.startsWith("  >-->")) {
					// Report if there was a previous one.
					if (currentErrorDetails.length > 0) {
						this.reportError(currentErrorHeading, currentErrorDetails)
						currentErrorHeading = []
					}
					currentErrorDetails = [line]
				}
				// If we were already reading the details, it means we've found the location info of the next error.
				else if (currentErrorDetails.length > 0) {
					this.reportError(currentErrorHeading, currentErrorDetails)
					currentErrorHeading = [line]
					currentErrorDetails = []
				}
				else {
					// We are reading the location info of the current error.
					currentErrorHeading.push(line)
				}
			}
			else {
				// We are still reading the details of the current error.
				currentErrorDetails.push(line)
			}
		}
		if (currentErrorDetails.length > 0) {
			this.reportError(currentErrorHeading, currentErrorDetails)
		}
	}

	reportError(headingLines: string[], detailsLines: string[]) {
		const original = [...headingLines, ...detailsLines].join("\r\n")
		/* Since the output can span multiple indented lines,
		we collapse new lines followed by a run of spaces into a single space.

		Known issue: when ni breaks a line, it only prints a new line, the space
		between words is not printed. This is usually not a problem (we replace
		new lines with spaces) but it can be if the break occurs on consecutive
		spaces (e.g. "foo    bar"). We can't know how many spaces were between
		"foo" and "bar". If this occurs in a file path, VS Code will receive an
		incorrect file path and the error won't be correctly reported. It will
		appear in the "Problems" panel but not in the editor and clicking on it
		will trigger an error. */
		const heading = headingLines.map(line => line.trimStart()).join(" ")
		const message = detailsLines.map(line => line.trimStart()).join(" ").replace(/^>-->\s*/, "")

		/* If a single error points to multiple locations in the source,
		the same error will be reported several times, one for each location. */
		const lineRe = / \((?<filePath>[^)]+?), line (?<line>\d+)\)/g
		let match = lineRe.exec(message)
		if (!match) {
			this.errorEmitter.fire({
				original,
				heading: heading,
				message: message
			})
			return
		}
		while (match?.groups) {
			// We remove the matched location from the message.
			const messageWithoutLocation = message.substring(0, match.index) + message.substring(lineRe.lastIndex)
			this.errorEmitter.fire({
				original,
				heading,
				message: messageWithoutLocation,
				location: {
					filePath: match.groups.filePath,
					line: match.groups.line
				}
			})
			// Try to match another location.
			match = lineRe.exec(message)
		}
	}

}
