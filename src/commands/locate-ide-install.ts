import path = require("path")

import * as vscode from "vscode"

import { globalStorage } from "../global-storage"
import { Command } from "../command-manager"
import { ProcessManager } from "../process-manager"


/**
 * Command that tries to locate the Inform 7 installation.
 *
 * @param uri The uri of the file to compile.
 */
export class LocateIdeInstallCommand implements Command {
	// TODO: I don't like that it's duplicated, but it will do.
	// It needs to be static so that you can access it without instantiating,
	// but it needs to be accessible on instances too.
	static readonly ID = "inform7.locateIdeInstall"
	readonly id = "inform7.locateIdeInstall"

	private processManager: ProcessManager

	constructor(processManager: ProcessManager) {
		this.processManager = processManager
	}

	/** Resolves to false if failed, true otherwise. */
	execute() {
		return new Promise((resolve, _reject) => {
			if (process.platform !== "linux") {
				vscode.window.showErrorMessage("The `inform7.locateIdeInstall` command is not supposed to be available when not on Linux. Please report this bug.")
				resolve(false)
				return
			}

			const child = this.processManager.spawn(
				"flatpak",
				["info", "--show-location", "com.inform7.IDE"],
				{},
			)

			// Flatpak command not installed.
			child.on("error", _err => {
				vscode.window.showWarningMessage("Flatpak does not seem to be installed. The Inform 7 extension relies on the Inform IDE FlatPak, so you won't be able to compile Inform projects.")
				resolve(false)
				return
			})

			// IDE not installed.
			child.stderr?.on("data", _data => {
				vscode.window.showWarningMessage("The Inform 7 IDE Flatpak does not seem to be installed. You won't be able to compile Inform projects.")
				resolve(false)
				return
			})

			// IDE installed. Success!
			child.stdout?.on("data", data => {
				const installPath = (data as Buffer).toString("utf-8").trim()
				globalStorage.ideInstallLocation = installPath
				resolve(true)
				return
			})
		})

	}
}
