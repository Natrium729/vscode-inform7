import * as vscode from "vscode"

import { CommandManager } from "./command-manager"
import { LocateIdeInstallCommand } from "./commands/locate-ide-install"
import { ProcessManager } from "./process-manager"
import { getNiPath } from "./inform-paths"
import { InformProjectManager } from "./inform-project"
import { TaskManager } from "./task-manager"
import { NiTaskProvider } from "./task-providers/ni-task-provider"
import { Inform6nTaskProvider } from "./task-providers/inform6n-task-provider"
import { CblorbTaskProvider } from "./task-providers/cblorb-task-provider"
import { FoldingProvider } from "./folding-provider"
import { globalStorage } from "./global-storage"

export function activate(context: vscode.ExtensionContext): void {
	const processManager = new ProcessManager()
	context.subscriptions.push(processManager)

	const informProjectManager = new InformProjectManager()
	context.subscriptions.push(informProjectManager)

	const taskManager = new TaskManager(processManager, informProjectManager)
	context.subscriptions.push(taskManager)

	const niTaskProvider = vscode.tasks.registerTaskProvider(
		NiTaskProvider.NiTaskType,
		new NiTaskProvider(taskManager)
	)
	context.subscriptions.push(niTaskProvider)

	const inform6nTaskProvider = vscode.tasks.registerTaskProvider(
		Inform6nTaskProvider.Inform6nTaskType,
		new Inform6nTaskProvider(taskManager)
	)
	context.subscriptions.push(inform6nTaskProvider)

	const cblorbTaskProvider = vscode.tasks.registerTaskProvider(
		CblorbTaskProvider.CblorbTaskType,
		new CblorbTaskProvider(taskManager)
	)
	context.subscriptions.push(cblorbTaskProvider)

	const commandManager = new CommandManager(taskManager, processManager)
	context.subscriptions.push(commandManager)

	locateIdeInstall(processManager)

	const foldingProvider = new FoldingProvider()
	context.subscriptions.push(foldingProvider)
}

function locateIdeInstall(processManager: ProcessManager) {
	if (process.platform === "linux") {
		// Locating the IDE Flatpak install is only relevant on Linux.
		vscode.commands.executeCommand(LocateIdeInstallCommand.ID)
			.then(() => getInstalledInformVersion())
	} else {
		// The Inform installation is at a fixed place on systems other than Linux,
		// so no need to locate it.
		getInstalledInformVersion()
	}

	function getInstalledInformVersion() {
		const niPath = getNiPath()
		if (!niPath) {
			// Happens when Inform is not installed.
			// It's actually unreachable
			// because we only run this function
			// after locating the install.
			return
		}

		const child = processManager.spawn(niPath, ["--version"], {})

		// Inform is not installed
		// (macOS and Windows, Linux was handled when locating the installation),
		// or maybe we lack permission.
		child.on("error", _err => {
			vscode.window.showWarningMessage("The Inform compiler could not be launched. Make sure it is installed if you want to be able to compile stories.")
		})

		child.stdout?.on("data", data => {
			const out = (data as Buffer).toString("utf-8").trim()
			const match = out.match(/version\s+(\S+)/)
			if (match) {
				globalStorage.installedInformVersion = match[1]
			}
		})
	}
}
