// Test texts are in a separate file to start at an indentation of 0 without making the tests ugly.

export const five_headings_1 = `
Volume 1
`

export const five_headings_2 = `
Book 1
`

export const five_headings_3 = `
Section A - this is a section
`

export const five_headings_4 = `
    Chapter -
`

export const five_headings_5 = `
    Part foobar
`

export const nested_headings = `
Volume 1

The kitchen is a room.

Book A

Include some extension by some developer.

Book B

[book B is empty]

Volume 2

Book 1

Part one

Chapter I

Section #1

The table is in the kitchen.
`

export const empty_heading = `
    Book 1 [will not fold because it's empty]

    Book 2 [will fold because we include the last line]
`

export const indented = `
When play begins:
    say "hello".

A post-check rule (this is the dry glove rule):
    if we get wet:
        if the player wears the gloves and the gloves are dry:
            now the gloves are sodden;
            say "(soaking your gloves in the process)";
    continue the action.
`

export const mixed_indentation = `
When play begins:
\tsay "this line is indented with a tab";
    say "but this on is indented with spaces".
`

export const line_endings_lf = `Volume 1\n\nPlease fold me.\n`

export const line_endings_crlf = `Volume 1\r\n\r\nPlease fold me.\r\n`

export const range_kind_region = `
Volume 1

This volume is a region.

Book 1

This book too.
`

export const range_kind_undefined = `
Simply indented:
    This paragraph is just indented code, not a region;
    And here's one more line.
`

export const floating_paragraph = `
This paragraph is fine.

    This one is indented but has no parent, because of the blank line.
    We call it a floating paragraph and do not create a range.
        This one is attached to is parent, it's not floating so we create a range.
`
