import * as vscode from "vscode"
import { FoldingProvider } from "."
import * as data from "./test-data"
import { TestDocument } from "../test/TestDocument"

// Some fake objects to pass type checks.
const mockDisposable: vscode.Disposable = {
	dispose: jest.fn(),
}
jest.spyOn(vscode.languages, "registerFoldingRangeProvider").mockReturnValue(
	mockDisposable
)
const context: vscode.FoldingContext = {}
const token = {} as vscode.CancellationToken

// Utility function to fold strings.
function fold(
	str: string,
	provider = new FoldingProvider()
): vscode.FoldingRange[] {
	const document = new TestDocument(vscode.Uri.file("test.ni"), str)
	const ranges = provider.provideFoldingRanges(document, context, token)
	// Sort for easier testing.
	ranges.sort((a, b) => {
		const startDiff = a.start - b.start
		return startDiff === 0 ? a.end - b.end : startDiff
	})
	return ranges
}

interface RangeToMatch {
	start: number
	end: number
	kind?: undefined | vscode.FoldingRangeKind
}
// Utility to make expectations more readable.
// Range kind will be checked only if specified.
function range(start: number, end: number, kind?: vscode.FoldingRangeKind) {
	const rangeToMatch: RangeToMatch = { start, end }
	// Only match kind if explicitly given.
	// Checking args.length allows detecting an explicit kind = undefined.
	if (arguments.length === 3) {
		rangeToMatch.kind = kind
	}
	return expect.objectContaining(rangeToMatch)
}

describe("FoldingProvider", () => {
	test("it folds all 5 headings", () => {
		expect(fold(data.five_headings_1)).toEqual([range(1, 2)])
		expect(fold(data.five_headings_2)).toEqual([range(1, 2)])
		expect(fold(data.five_headings_3)).toEqual([range(1, 2)])
		expect(fold(data.five_headings_4)).toEqual([range(1, 2)])
		expect(fold(data.five_headings_5)).toEqual([range(1, 2)])
	})

	test("it folds nested headings", () => {
		expect(fold(data.nested_headings)).toEqual([
			range(1, 11), // Volume 1
			range(5, 7), // Book A
			range(9, 11), // Book B
			range(13, 24), // Volume 2
			range(15, 24), // Book 1
			range(17, 24), // Part 1
			range(19, 24), // Chapter I
			range(21, 24), // Section #1
		])
	})

	test("it does not fold if the heading is empty", () => {
		// Book 1 doesn't fold since it's empty.
		// Book 2 does because we include the last line.
		expect(fold(data.empty_heading)).toEqual([range(3, 4)])
	})

	test("it folds indented blocks", () => {
		expect(fold(data.indented)).toEqual([
			range(1, 2),
			range(4, 10),
			range(5, 8),
			range(6, 8),
		])
	})

	test("it handles mixed spaces & tabs", () => {
		const provider = new FoldingProvider()
		jest.spyOn(provider, "getTabSize").mockReturnValue(4)

		expect(fold(data.mixed_indentation, provider)).toEqual([range(1, 4)])
	})

	test("it handles different line endings", () => {
		expect(fold(data.line_endings_lf)).toEqual([range(0, 3)])
		expect(fold(data.line_endings_crlf)).toEqual([range(0, 3)])
	})

	test("it marks heading ranges as regions", () => {
		expect(fold(data.range_kind_region)).toEqual([
			range(1, 8, vscode.FoldingRangeKind.Region),
			range(5, 8, vscode.FoldingRangeKind.Region),
		])
	})

	test("it marks indented ranges with no kind", () => {
		expect(fold(data.range_kind_undefined)).toEqual([
			range(1, 4, undefined),
		])
	})

	test("it does not fold floating paragraphs", () => {
		expect(fold(data.floating_paragraph)).toEqual([range(4, 6)])
	})
})
