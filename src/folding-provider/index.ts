import * as vscode from "vscode"

// Inform has 5 types of heading, here in hierarchical order.
const headings = ["volume", "book", "part", "chapter", "section"]

type RangeLevel = string | number // strings for headings, numbers for indentation.

type RangeInProgress = {
	level: RangeLevel
	start: number
}

// Matches an Inform heading line.
const headingRegExp = new RegExp(
	"^\\s*(" + headings.join("|") + ")\\s+.*$",
	"i"
)

// Tells if a range should be marked as a FoldingRangeKind.Region in VS Code.
function isRegion(level: RangeLevel) {
	return typeof level === "string"
}

export class FoldingProvider
	implements vscode.FoldingRangeProvider, vscode.Disposable
{
	registeredDisposable: vscode.Disposable

	constructor() {
		// This provider can handle inform & inform7extension languages.
		this.registeredDisposable =
			vscode.languages.registerFoldingRangeProvider(
				["inform7", "inform7extension"],
				this
			)
	}

	dispose() {
		this.registeredDisposable.dispose()
	}

	provideFoldingRanges(
		document: vscode.TextDocument,
		context: vscode.FoldingContext,
		token: vscode.CancellationToken
	): vscode.FoldingRange[] {
		const ranges: vscode.FoldingRange[] = []
		// Keep track of the ranges in progress, headings or indentation.
		const rangesInProgress: RangeInProgress[] = []
		let previousRangeLevel: RangeLevel | null = null
		// Get tab size, used to compute mixed indentation.
		const tabSize = this.getTabSize()

		for (let lineNo = 0; lineNo < document.lineCount; lineNo++) {
			// If the last line of a range is blank, do not include it.
			const isPreviousLineBlank =
				lineNo > 0 && document.lineAt(lineNo - 1).isEmptyOrWhitespace
			const endLine = isPreviousLineBlank ? lineNo - 2 : lineNo - 1

			const rangeLevel = this.getRangeLevel(document, lineNo, tabSize)
			// If it's a heading:
			if (typeof rangeLevel === "string") {
				// end all started ranges (headings or indents) at this level and deeper,
				this.endDeeperRanges(
					rangeLevel,
					endLine,
					rangesInProgress,
					ranges
				)
				// and start the next range.
				rangesInProgress.push({
					level: rangeLevel,
					start: lineNo,
				})
			}
			// Or if it's an indented line of different level:
			if (
				typeof rangeLevel === "number" &&
				rangeLevel !== -1 && // Ignore blank lines.
				previousRangeLevel !== rangeLevel
			) {
				// end all started ranges (indents) strictly deeper,
				this.endDeeperRanges(
					rangeLevel + 1, // Strictly deeper.
					endLine,
					rangesInProgress,
					ranges
				)
				// and if indentation increased, start the next range at the previous line, only if it's not blank.
				if (
					typeof previousRangeLevel === "number" &&
					rangeLevel > previousRangeLevel &&
					!document.lineAt(lineNo - 1).isEmptyOrWhitespace
				) {
					rangesInProgress.push({
						level: rangeLevel,
						start: lineNo - 1,
					})
				}
			}
			// If it was not a blank line, update the previous range level.
			if (rangeLevel !== -1) {
				previousRangeLevel = rangeLevel
			}
		}
		// We have reached the end of the text, we need to end all remaining ranges.
		// Here, we include the last line even if blank.
		this.endDeeperRanges(
			"volume", // Highest level.
			document.lineCount - 1,
			rangesInProgress,
			ranges
		)

		return ranges
	}

	getTabSize(): number {
		const editor = vscode.window.activeTextEditor
		return (editor?.options.tabSize as number) ?? 4
	}

	// Returns the range level of a line or -1 if it's a blank line.
	getRangeLevel(
		document: vscode.TextDocument,
		lineNo: number,
		tabSize: number
	): RangeLevel {
		const prevLine = lineNo > 0 ? document.lineAt(lineNo - 1) : null
		const line = document.lineAt(lineNo)
		const nextLine =
			lineNo + 1 < document.lineCount ? document.lineAt(lineNo + 1) : null

		const indentation = computeIndentLevel(line.text, tabSize)

		// A heading must be preceded by a blank line (except on the first line).
		if (prevLine && !prevLine.isEmptyOrWhitespace) {
			return indentation
		}
		// And followed by one (except if it on the last line).
		if (nextLine && !nextLine.isEmptyOrWhitespace) {
			return indentation
		}
		// Finally, it must match the RegExp.
		const results = headingRegExp.exec(line.text)
		if (results) {
			return results[1].toLowerCase()
		}
		return indentation
	}

	// End all ranges of same level or lower.
	endDeeperRanges(
		level: RangeLevel,
		endLine: number,
		rangesInProgress: RangeInProgress[],
		ranges: vscode.FoldingRange[]
	) {
		if (rangesInProgress.length === 0) {
			return
		}
		let range = rangesInProgress[rangesInProgress.length - 1]
		// Work our way up until we reach the given level.
		while (range != null && compareLevels(level, range.level) <= 0) {
			// Create a range if not empty.
			if (range.start < endLine) {
				ranges.push(
					new vscode.FoldingRange(
						range.start,
						endLine,
						isRegion(range.level)
							? vscode.FoldingRangeKind.Region
							: undefined // Indented code is not a region.
					)
				)
			}
			rangesInProgress.pop()
			range = rangesInProgress[rangesInProgress.length - 1]
		}
	}
}

// Compares RangeLevels.
// volume < book < part < chapter < section < indent 1 < indent 2 < …
function compareLevels(a: RangeLevel, b: RangeLevel): number {
	if (typeof a === "string") {
		if (typeof b === "string") {
			// compare heading levels
			return headings.indexOf(a) - headings.indexOf(b)
		} else {
			return -1 // a (heading) < b (indent)
		}
	} else {
		if (typeof b === "string") {
			return 1 // a (indent) > b (heading)
		} else {
			return a - b
		}
	}
}

// Computes indentation level and deals with mixed tabs/spaces.
// Adapted from VS Code source (MIT license):
// https://github.com/microsoft/vscode/blob/15af2df708b25f53c8b1c7ff4feab1746cbe04d2/src/vs/editor/common/model/textModel.ts#L2725
function computeIndentLevel(line: string, tabSize: number): number {
	let indent = 0
	let i = 0
	const len = line.length

	while (i < len) {
		const chCode = line.charCodeAt(i)
		if (chCode === 32) {
			indent++
		} else if (chCode === 9) {
			indent = indent - (indent % tabSize) + tabSize
		} else {
			break
		}
		i++
	}

	if (i === len) {
		return -1 // Line only consists of whitespace.
	}

	return indent
}
