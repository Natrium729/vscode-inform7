import os = require("os")
import path = require("path")

import * as vscode from "vscode"

import { globalStorage } from "./global-storage"


/**
 * Check if a fully-specified version (e.g. "x.y.z")
 * matches a version without a patch number (e.g. "x.y").
 *
 * So "10.1.2" matches "10.1", but doesn't match "10.0" nor "9.3".
 *
 * If an argument is an invalid version (e.g. "6L38"), false is returned.
*/
function fullVersionMatchesMinor(version1: string, version2: string) {
	const split1 = version1.split(".")
	if (split1.length < 2) {
		return false
	}

	const split2 = version2.split(".")
	if (split2.length < 2) {
		return false
	}

	return split1[0] === split2[0] && split1[1] === split2[1]
}


/**
 * Get the path of the Inform 7 compiler for the given version.
 *
 * If `version` is not given, the path will be for the latest version available.
 */
export function getNiPath(version?: string) {
	const customNiPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("ni", "")
	if (customNiPath) {
		return customNiPath
	}

	switch (process.platform) {
		case "darwin":
			return version ?
				`/Applications/Inform.app/Contents/MacOS/${version}/ni`
				: "/Applications/Inform.app/Contents/MacOS/ni"

		case "linux": {
			const ideInstallPath = globalStorage.ideInstallLocation
			const installedVersion = globalStorage.installedInformVersion

			// The Inform IDE is not installed.
			if (!ideInstallPath || !installedVersion) {
				return null
			}

			// We're asking for the latest version of Inform.
			// It will always be 10.1 and later.
			// since we don't support the IDE from 6M62 and earlier.
			// So the executable is always named `inform7` (and never `ni`).
			if (!version || fullVersionMatchesMinor(installedVersion, version)) {
				return path.join(ideInstallPath, "files/libexec/inform7-ide/inform7")
			}

			// We're asking for a retrospective version.
			const binName = /\d[a-zA-Z]\d\d/.test(version) ? "ni" : "inform7"
			return path.join(ideInstallPath, "files/libexec/inform7-ide/retrospective", version, binName)
		}

		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\ni.exe"

		default:
			return "ni"
	}
}


export function getInform6Path() {
	const customInform6Path = vscode.workspace.getConfiguration("inform7.paths").get<string>("inform6", "")
	if (customInform6Path) {
		return customInform6Path
	}

	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/inform6"

		case "linux": {
			const ideInstallPath = globalStorage.ideInstallLocation

			// The Inform IDE is not installed.
			if (!ideInstallPath) {
				return null
			}

			return path.join(ideInstallPath, "files/libexec/inform7-ide/inform6")
		}

		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\inform6.exe"
		default:
			return "inform6"
	}
}

/**
 * Get the path of the inblorb/cBlorb executable for the given version.
 *
 * If `version` is not given, the path will be for the latest version available.
 */
export function getCblorbPath(version?: string) {
	const customCblorbPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("cblorb", "")
	if (customCblorbPath) {
		return customCblorbPath
	}
	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/cBlorb"

		case "linux": {
			const ideInstallPath = globalStorage.ideInstallLocation
			const installedVersion = globalStorage.installedInformVersion

			// The Inform IDE is not installed.
			if (!ideInstallPath || !installedVersion) {
				return null
			}

			// We're asking for the latest version of Inform.
			// It will always be 10.1 and later.
			// since we don't support the IDE from 6M62 and earlier.
			// So the executable is always named `inblorb` (and never `ni`).
			if (!version || fullVersionMatchesMinor(installedVersion, version)) {
				return path.join(ideInstallPath, "files/libexec/inform7-ide/inblorb")
			}

			// We're asking for a retrospective version.
			const binName = /\d[a-zA-Z]\d\d/.test(version) ? "cBlorb" : "inblorb"
			return path.join(ideInstallPath, "files/libexec/inform7-ide/retrospective", version, binName)
		}

		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\cBlorb.exe"

		default:
			return "cBlorb"
	}
}


export function getInternalPath(version?: string) {
	const customInternalPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("internal", "")
	if (customInternalPath) {
		return customInternalPath
	}
	switch (process.platform) {
		case "darwin":
			return `/Applications/Inform.app/Contents/Resources/retrospective/${version || "6M62"}`
		case "linux":
			return "/usr/local/share/inform7/Internal"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Internal"
		default:
			return ""
	}
}


export function getExternalPath() {
	const customExternalPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("external", "")
	if (customExternalPath) {
		return customExternalPath
	}
	switch (process.platform) {
		case "darwin":
			return path.join(os.homedir(), "/Library/Inform")
		case "linux":
			return path.join(os.homedir(), "Inform")
		case "win32":
			return path.join(os.userInfo().homedir, "Documents\\Inform")
		default:
			return path.join(os.homedir(), "Inform")
	}
}
